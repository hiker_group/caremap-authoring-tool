<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace common\models\base;

use Yii;

/**
 * This is the base-model class for table "node_note".
 *
 * @property integer $node_id
 * @property integer $note_id
 *
 * @property \common\models\Node $node
 * @property \common\models\Note $note
 * @property string $aliasModel
 */
abstract class NodeNote extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'node_note';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['node_id', 'note_id'], 'required'],
            [['node_id', 'note_id'], 'integer'],
            [['node_id'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['node_id' => 'id']],
            [['note_id'], 'exist', 'skipOnError' => true, 'targetClass' => Note::className(), 'targetAttribute' => ['note_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'node_id' => Yii::t('app', 'Node ID'),
            'note_id' => Yii::t('app', 'Note ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNode()
    {
        return $this->hasOne(\common\models\Node::className(), ['id' => 'node_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNote()
    {
        return $this->hasOne(\common\models\Note::className(), ['id' => 'note_id']);
    }




}
