<?php

namespace common\widgets\authoring;

use yii\web\AssetBundle;

class AuthoringAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/authoring';

    public $js = [
        'authoring.jsx',
    ];

    public $css = [
        'authoring.css',
    ];

    public $jsOptions = [
        'type' => "text/babel"
    ];

    public $depends = [
        'common\widgets\react\ReactAsset',
        'common\widgets\authoring\CytoscapeDagreAsset',
        'common\widgets\authoring\CytoscapeAsset',
        'yii\web\JqueryAsset',
    ];
}
