<?php

namespace common\widgets\authoring;

use yii\web\AssetBundle;

class CytoscapeAsset extends AssetBundle
{
    public $sourcePath = '@bower/cytoscape/dist';

    public $js = [
        'cytoscape.js',
    ];
}
