<?php

namespace common\widgets\authoring;

use yii\web\AssetBundle;

class CytoscapeDagreAsset extends AssetBundle
{
    public $sourcePath = '@bower/cytoscape-dagre';

    public $js = [
        'cytoscape-dagre.js',
    ];
}
