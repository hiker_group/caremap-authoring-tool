var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
var update = React.addons.update;

var Cytoscape = React.createClass({
  cy: null,

  componentDidMount() {
    console.log(this.props.elements);
    var conf = {
      style: cytoscape.stylesheet()
        .selector('node')
        .css({
          'cursor': 'pointer',
          'content': 'data(id)'
        })        
        .selector('edge')
        .css({
          'width': 4,
          'target-arrow-shape': 'triangle',
          'cursor': 'pointer',
          // 'line-color': '#9dbaea',
          // 'target-arrow-color': '#9dbaea',
          'curve-style': 'bezier'
        }),
      elements: this.props.elements,
      layout: {
        name: 'breadthfirst',
        // fit: false,
        spacingFactor: 0.95,
        roots: ['Patients']
      }
    };
    conf.container = this.refs.cyelement;
    let cy = cytoscape(conf);

    this.cy = cy;

    cy.on('select', function(event){
      console.log(event);
    }.bind(this));

    cy.on('tap', function(event){

      // cyTarget holds a reference to the originator
      // of the event (core or element)
      var evtTarget = event.cyTarget;
      console.log(evtTarget);
      if( evtTarget === cy ){
        this.props.selectComponent(undefined);  
      } else {
        this.props.selectComponent(evtTarget.id());
        console.log('tap on some element' + evtTarget.id());
      }
    }.bind(this));
  },

  shouldComponentUpdate(){
    return false;
  },

  componentWillReceiveProps(nextProps){
    this.cy.json({elements: nextProps.elements});
  },

  componentWillUnmount() {
    this.cy.destroy();
  },

  getCy() {
    return this.cy;
  },

  render() {
    return <div id="cy" className="graph" ref="cyelement" />
  }
});

var CareMaps =  React.createClass({
  handleSelectCareMap(id) {
    this.props.selectCareMap(id);
  },
 
  handleDeleteCareMap(id) {
    this.props.setInactive(id);
  },

  render() {
    var careMaps = this.props.careMaps.filter(function(careMap) {
      return careMap.active;
    });

    var careMapCards = careMaps.map(function(careMap) {
      var inactive = false;
      if (!careMap.id) {
        inactive = true;
        careMap.id = 'NEW';
      }

      return (
        <div key={careMap.id} className={'caremap-card' + (inactive ? ' inactive' : '')}>
          <div className="caremap-card-details">
            <h3 className='caremap-heading'>{careMap.name}</h3>
            <div className="caremap-description">{careMap.description}</div>
          </div>
          <div className="caremap-card-actions">
            <a href="#" onClick={this.handleSelectCareMap.bind(this, careMap.id)}>View</a>
            <a href="#">Edit</a>
            <a href="#" className="delete" onClick={this.handleDeleteCareMap.bind(this, careMap.id)}>Delete</a>
          </div>
        </div>
      );
    }.bind(this));

    if (!careMapCards.length && this.props.loaded) {
      careMapCards = 
        <div className='caremap-card'>
          <div className="caremap-card-details text-center">
            <div>No CareMaps</div>
            <div>Create a CareMap to start</div>
          </div>
        </div>;
    }

    return (
      <div className="caremap-container">{careMapCards}</div>
    );
  }
});

var CareMapForm =  React.createClass({
  getInitialState() {
    return { name: '', description: ''};
  },

  handleNameChange(e) {
    this.setState({name: e.target.value});
  },

  handleDescriptionChange(e) {
    this.setState({description: e.target.value});
  },

  submit(e) {
    e.preventDefault();
    if (this.props.saving) {
      return;
    }
    console.log(this.state.name, this.state.description);
    this.props.onSubmit(this.state.name, this.state.description);
    this.setState({name: '', description: ''});
  },

  render() {
    return (
      <div className="care-map">
        <form onSubmit={this.submit}>
          <div className="form-group">
            <label className="control-label" htmlFor="caremap-name">Name</label>
            <input type="text" className="form-control" onChange={this.handleNameChange} value={this.state.name} />
          </div>
          <div className="form-group">
            <label className="control-label" htmlFor="caremap-description">Description</label>
            <textarea id="caremap-description" className="form-control" rows="6" onChange={this.handleDescriptionChange} value={this.state.description} />
          </div>
          <div className="form-group">
            <button disabled={this.props.saving} type="submit" className="btn btn-success">Create CareMap</button>
          </div>
        </form>
      </div>
    );
  }
});

var LinkForm = React.createClass({
  submit(e) {
    e.preventDefault();
    var value = this.state.value
    if (!value) return;
    
    if (this.state.selectedIsFrom) {
      this.props.addLink(this.props.selected, value);
    } else {
      this.props.addLink(value, this.props.selected);
    }
  },

  switchDirection() {
    this.setState({
      selectedIsFrom: !this.state.selectedIsFrom
    });
  },

  onChange(e) {
    this.setState({
      value: e.target.value
    })
  },

  getInitialState() {
    return ({
      value: this.props.elements.nodes[0].data.id,
      selectedIsFrom: true
    });
  },

  render() {
    var linkOptions = this.props.elements.nodes.map(function(node) {
      return <option key={node.data.id}>{node.data.id}</option>;
    }) ;

    var direction;
    if (this.state.selectedIsFrom) {
      direction = "glyphicon glyphicon-arrow-right";
    } else {
      direction = "glyphicon glyphicon-arrow-left";
    }

    return (
      <div className="care-map">
        <h3>Links</h3>
        <form onSubmit={this.submit}>
          <div className="form-group">
            <select value={this.state.value} onChange={this.onChange} className="form-control">
              {linkOptions}
            </select>
          </div>
          {this.props.selected}
          <a href="#" onClick={this.switchDirection}>
            <span className={direction}></span>
          </a>
          {this.state.value}
          <div className="form-group">
            <button type="submit" className="btn btn-success">Add Link</button>
          </div>
        </form>
      </div>
    );
  }
});

var AddComponent = React.createClass({
  getInitialState() {
    return { name: ''};
  },

  handleNameChange(e) {
    this.setState({name: e.target.value});
  },

  submit(e) {
    e.preventDefault();
    if (!this.state.name) {
      // todo - warning
      return;
    }
    this.props.onSubmit({
      name: this.state.name,
    });

    this.setState({name: ''});
  },

  render() {
    return (
      <div className="care-map">
        <form onSubmit={this.submit} style={{maxWidth: 330}}>
          <div className="form-group">
            <div className="input-group">
              <input
                type="text"
                className="form-control"
                onChange={this.handleNameChange}
                value={this.state.name} placeholder="Enter name..."
              />
              <span className="input-group-btn">
                <button className="btn btn-default" type="submit">Add</button>
              </span>
            </div>
          </div>
        </form>
      </div>
    );
  }
});

var Notes = React.createClass({
  getInitialState() {
    return { title: '', description: '', content: ''};
  },

  handleTitleChange(e) {
    this.setState({title: e.target.value});
  },

  handleDescriptionChange(e) {
    this.setState({description: e.target.value});
  },

  handleContentChange(e) {
    this.setState({content: e.target.value});
  },

  submit(e) {
    e.preventDefault();
    if (!this.state.title || !this.state.content) {
      // todo - warning
      return;
    }

    this.props.onSubmit({
      title: this.state.title,
      description: this.state.description,
      content: this.state.content
    });

    this.setState({ title: '', description: '', content: ''});
  },

  render() {
    var noteCards = this.props.notes.map(function(note, index) {
      return (
        <div key={index} className='caremap-card'>
          <div className="caremap-card-details">
            <h3 className='caremap-heading'>{note.title}</h3>
            <div className="caremap-description">
              <div style={{marginBottom: 15}}>{note.description}</div>
              <div>{note.content}</div>
            </div>
          </div>
          <div className="caremap-card-actions">
            <a href="#">Edit</a>
            <a href="#" className="delete">Delete</a>
          </div>
        </div>
      );
    }.bind(this));


    if (!noteCards.length) {
      noteCards = 
        <div className='caremap-card'>
          <div className="caremap-card-details text-center">
            <div>No notes</div>
          </div>
        </div>;
    }

    return (
      <div className="notes">
        <h3>Notes</h3>
        <div className='caremap-container'>
          {noteCards}
        </div>
        <form onSubmit={this.submit}>
          <div className="form-group">
            <label className="control-label">Title</label>
            <input type="text" className="form-control" onChange={this.handleTitleChange} value={this.state.title} />
          </div>

          <div className="form-group">
            <label className="control-label">Description</label>
            <input type="text" className="form-control" onChange={this.handleDescriptionChange} value={this.state.description} />
          </div>
          <div className="form-group">
            <label className="control-label">Content</label>
            <textarea id="caremap-description" className="form-control" rows="6" onChange={this.handleContentChange} value={this.state.content} />
          </div>
          <div className="form-group">
            <button disabled={this.props.saving} type="submit" className="btn btn-success">Create CareMap</button>
          </div>
        </form>
      </div>
    );
  }
});

var ComponentForm = React.createClass({
  getInitialState() {
    return { name: '', description: ''};
  },

  handleNameChange(e) {
    this.setState({name: e.target.value});
  },

  handleDescriptionChange(e) {
    this.setState({description: e.target.value});
  },

  submit(e) {
    e.preventDefault();
    if (!this.state.name) {
      // todo - warning
      return;
    }
    this.props.onSubmit({
      name: this.state.name,
      description: this.state.description
    });

    this.setState({name: '', description: ''});
  },

  render() {
    var title, edit = this.props.edit;
    
    if (edit) {
      title = 'Edit Component';
    } else {
      title = 'Add Component';
    }

    return (
      <div className="care-map">
        <h3>{title}</h3>
        <form onSubmit={this.submit}>
          <div className="form-group">
            <label className="control-label" htmlFor="caremap-name">Name</label>
            <input type="text" className="form-control" onChange={this.handleNameChange} value={this.state.name} />
          </div>
          <div className="form-group">
            <label className="control-label" htmlFor="caremap-description">Description</label>
            <textarea id="caremap-description" className="form-control" rows="6" onChange={this.handleDescriptionChange} value={this.state.description} />
          </div>
          <div className="form-group">
            <div className="btn-toolbar">
              <button type="submit" className="btn btn-success">{edit ? 'Edit Component' : 'Add Component'}</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
});

var EditCareMap = React.createClass({
  deselectComponent() {
    this.setState({selectedComponent: undefined});
  },

  selectComponent(name) {
    this.setState({selectedComponent: name});
  },

  deleteComponent(id) {
    var nodes = this.state.elements.nodes;

    for (var i = 0; i < nodes.length; i++) {
      if (nodes[i].data.id == id) {
        var newElements = update(this.state.elements, {nodes: {$splice: [[i, 1]]}});
        // 
        this.setState({elements: newElements, selectedComponent: undefined});
      }
    }

    var edges = this.state.elements.edges;

    for (var i = 0; i < edges.length; i++) {
      if (edges[i].data.id == id) {
        var newElements = update(this.state.elements, {edges: {$splice: [[i, 1]]}});
        // 
        this.setState({elements: newElements, selectedComponent: undefined});
      }
    }
    

    this.findLinks(id);
  },

  findLinks(name) {
    var links = [], edge, edges = this.state.elements.edges;
    for (var i = 0; i < edges.length; i++) {
      edge = edges[i];
      if (edge.data.source == name) {
        console.log(edge);
      } else if (edge.data.target == name) {
        console.log(edge);
      }      
    }
  },

  editComponent(name) {
    return;
  },

  addComponent(component) {
    // var newElements = update(this.state.elements, {nodes: {$push: [ { data: {"id": component.name } }]}})
    // this.setState({elements: newElements});
    // this.refs.graph.getCy().json({elements: newElements});
    // this.refs.graph.getCy().add(component);
    // this.refs.graph.getCy().center();
    // console.log(this.props.careMap);

    $.ajax({
      url: this.props.saveNodeUrl,
      type: 'POST',
      dataType: 'json',
      data: {
        Node: {
          care_map_id: this.props.careMap.id,
          name: component.name,
          description: component.description
        }
      },
      success: function(data) {
        var node, nodes = data.nodes, newNodes = [];
        for (var i = 0; i < nodes.length; i++) {
          node = nodes[i];
          newNodes.push({data: {id: nodes[i].name}});
        }

        var newElements = update(this.state.elements, {nodes: {$set: newNodes}});

        this.setState({
          elements: newElements
        })

        this.refs.graph.getCy().json({elements: newElements});

        // TODO unsuccessful save?
        // this.setState({
        //   careMaps: data.careMaps,
        //   savingCareMap: undefined
        // });
      }.bind(this),
      error: function(xhr, status, err) {
        // kill the program
        // this.setState({
        //   savingCareMap: undefined
        // });
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },

  addLink(fromComponent, toComponent) {
    var component = {
      data: {"source": fromComponent, target: toComponent }
    };
    var newElements = update(this.state.elements, {edges: {$push: [component]}})
    this.setState({elements: newElements});
    this.refs.graph.getCy().add(component);
    // this.refs.graph.getCy().center();
  },

  addNodeNote(note) {
    note.nodeName = this.state.selectedComponent;

    this.setState({
      notes: update(this.state.notes, { $push: [note]})  
    });
  },

  findNodeNotes(name) {
    var notes = this.state.notes,
        nodeNotes = [],
        note;

    console.log(notes);

    for (var i = 0; i < notes.length; i++) {
      note = notes[i];
      if (note.nodeName == name) {
        nodeNotes.push(note);
      }
    }

    return nodeNotes;
  },

  getInitialState() {
    return {
      elements: {nodes: [{ data: {"id": "Patients" } }], edges: []},
      notes: []
    }
  },

  render() {
    console.log(this.state.elements);
    var selectedComponent = this.state.selectedComponent;

    if (selectedComponent) {
      return (
        <div className="row">
          <div className="col-sm-7">
            <Cytoscape selectComponent={this.selectComponent} ref="graph" elements={this.state.elements} />
          </div>
          <div className="col-sm-5">
            <h3>
              Edit Component: {selectedComponent}
              <div className="btn-toolbar pull-right">
                <button className="btn btn-default" onClick={this.deselectComponent}>Unselect</button>
                <button className="btn btn-danger" onClick={this.deleteComponent.bind(this, selectedComponent.id)}>Delete</button>
              </div>
            </h3>

            <div>
              <ul className="nav nav-tabs" role="tablist">
                <li role="presentation" className="active"><a href="#links" aria-controls="links" role="tab" data-toggle="tab">Links</a></li>
                <li role="presentation"><a href="#notes" aria-controls="notes" role="tab" data-toggle="tab">Notes</a></li>
                <li role="presentation"><a href="#edit" aria-controls="edit" role="tab" data-toggle="tab">Edit</a></li>
              </ul>

              <div className="tab-content">
                <div role="tabpanel" className="tab-pane" id="edit">
                  <ComponentForm
                    deleteComponent={this.deleteComponent.bind(this, selectedComponent.id)}
                    deselectComponent={this.deselectComponent}
                    edit={true}
                    onSubmit={this.editComponent}
                  />
                </div>
                <div role="tabpanel" className="tab-pane active" id="links">
                  <LinkForm addLink={this.addLink} elements={this.state.elements} selected={selectedComponent} />
                </div>
                <div role="tabpanel" className="tab-pane" id="notes">
                  <Notes
                    notes={this.findNodeNotes(selectedComponent)}
                    onSubmit={this.addNodeNote}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="row">
          <div className="col-sm-7">
            <Cytoscape selectComponent={this.selectComponent} ref="graph" elements={this.state.elements} />
          </div>
          <div className="col-sm-5">
            <ComponentForm
              deleteComponent={this.deleteComponent}
              deselectComponent={this.deselectComponent}
              edit={false}
              onSubmit={this.addComponent}
            />
          </div>
        </div>
      );
    }
  }
});

var AuthoringTool = React.createClass({
  deselectCareMap(e) {
    e.preventDefault();
    this.setState({selected: undefined, savingCareMap: false});
  },

  selectCareMap(id) {
    this.setState({selected: id});
  },

  getInitialState() {
    return {
      careMaps: [],
      loaded: false
    }
  },

  saveCareMap(name, description) {
    this.setState({
      savingCareMap: name,
      careMaps: update(this.state.careMaps, { $push: [{name: name, description: description, active: true}]})
    });

    this.request = $.ajax({
      url: this.props.careMapsUrl,
      type: 'POST',
      dataType: 'json',
      data: {
        CareMap: {
          name: name,
          description: description
        }
      },
      success: function(data) {
        console.log(data);
        // TODO unsuccessful save?
        this.setState({
          careMaps: data.careMaps,
          savingCareMap: undefined
        });
      }.bind(this),
      error: function(xhr, status, err) {
        // kill the program
        this.setState({
          savingCareMap: undefined
        });
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  loadCareMaps() {
    this.request = $.ajax({
      url: this.props.careMapsUrl,
      dataType: 'json',
      cache: false,
      success: function(data) {
        console.log(data);
        this.setState({
          careMaps: data.careMaps,
          loaded: true
        });
      }.bind(this),
      error: function(xhr, status, err) {
        // kill the program
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },

  findCareMapIndex(id) {
    var careMaps = this.state.careMaps;
    for (var i = 0; i < careMaps.length; i++) {
      if (careMaps[i].id == id) {
        return i;
      }
    }
    return -1;
  },

  // delete(id) {
  //   var index = this.findCareMapIndex(id);
  //   var oldCareMap = this.state.careMaps[index];
  //   // check cm / del index
  //   this.setState({
  //     careMaps: update(this.state.careMaps, { $splice: [[index, 1]] }),
  //   });

  //   $.ajax({
  //     url: this.props.setInactive,
  //     type: 'POST',
  //     dataType: 'json',
  //     data: {
  //       id: id
  //     },
  //     success: function(data) {
  //       if (!data.success) {
  //         this.setState({
  //           careMaps: update(this.state.careMaps, { $push: [oldCareMap] }),
  //         });          
  //       } 
  //     }.bind(this),
  //     error: function(xhr, status, err) {
  //       this.setState({
  //         careMaps: update(this.state.careMaps, { $push: [oldCareMap] }),
  //       });
  //       console.error(xhr, status, err.toString());
  //     }.bind(this)
  //   });
  // },

  updateCareMapStatus(id, status) {
    var index = this.findCareMapIndex(id);
    var newCareMap = update(this.state.careMaps[index], {active: {$set: status}});

    this.setState({
      careMaps: update(this.state.careMaps, { $splice: [[index, 1, newCareMap]]})
    });    
  },

  setInactive(id) {
    this.updateCareMapStatus(id, 0);

    $.ajax({
      url: this.props.setInactive,
      type: 'POST',
      dataType: 'json',
      data: {
        id: id
      },
      success: function(data) {
        if (!data.success) {
          this.updateCareMapStatus(id, 1);
        } 
      }.bind(this),
      error: function(xhr, status, err) {
        this.updateCareMapStatus(id, 1);
        console.error(xhr, status, err.toString());
      }.bind(this)
    });
  },

  componentDidMount() {
    this.loadCareMaps();
  },

  getSelectedCareMap() {
    var careMaps = this.state.careMaps;
    console.log(careMaps);
    for (var i = 0; i < careMaps.length; i++) {
      if (careMaps[i].id == this.state.selected) {
        return careMaps[i];
      }
    }
  },

  render() {
    var content;

    if (this.state.selected) {
      var careMap = this.getSelectedCareMap();
      return (
        <div>
          <ol className="breadcrumb">
            <li><a href="#" onClick={this.deselectCareMap}>CareMaps</a></li>
            <li className="active">{careMap.name}</li>
          </ol>
          <h1>CareMap: {careMap.name}</h1>
          <EditCareMap careMap={careMap} saveNodeUrl={this.props.saveNodeUrl} />
        </div>
      );      
    } else {
      return (
        <div>
          <ol className="breadcrumb">
            <li className="active">CareMaps</li>
          </ol>
          <h1>CareMaps</h1>
          <div className="row">
            <div className="col-sm-5">
              <CareMaps 
                loaded={this.state.loaded}
                careMaps={this.state.careMaps}
                selectCareMap={this.selectCareMap}
                setInactive={this.setInactive}
              />
            </div>
            <div className="col-sm-7">
              <CareMapForm saving={this.state.savingCareMap} onSubmit={this.saveCareMap} />
            </div>
          </div>
        </div>
      );
    }
  }
});

var container = document.getElementById('content');

ReactDOM.render(
  <AuthoringTool 
    saveNodeUrl={container.getAttribute('data-save-node-url')}
    careMapsUrl={container.getAttribute('data-caremaps-url')}
    setInactive={container.getAttribute('data-set-inactive-url')}
    // autoApply={true}
  />,
  container
);
