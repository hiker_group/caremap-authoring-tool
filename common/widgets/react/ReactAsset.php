<?php

namespace common\widgets\react;

use yii\web\AssetBundle;

class ReactAsset extends AssetBundle {
  public $sourcePath = '@bower/react';

  public $css = [
  ];
  
  public $js = [
      'react-with-addons.js',
      'react-dom.js',
      'https://npmcdn.com/babel-core@5.8.38/browser.min.js'
  ];
}
