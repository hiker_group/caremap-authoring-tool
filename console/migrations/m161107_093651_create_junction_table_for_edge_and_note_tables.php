<?php

use yii\db\Migration;

/**
 * Handles the creation of table `edge_note`.
 * Has foreign keys to the tables:
 *
 * - `edge`
 * - `note`
 */
class m161107_093651_create_junction_table_for_edge_and_note_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('edge_note', [
            'edge_id' => $this->integer(),
            'note_id' => $this->integer(),
            'PRIMARY KEY(edge_id, note_id)',
        ], $tableOptions);


        // creates index for column `note_id`
        $this->createIndex(
            'idx-edge_note-note_id',
            'edge_note',
            'note_id'
        );

        // add foreign key for table `note`
        $this->addForeignKey(
            'fk-edge_note-note_id',
            'edge_note',
            'note_id',
            'note',
            'id',
            'CASCADE'
        );
        
        // creates index for column `note_id`
        $this->createIndex(
            'idx-edge_note-edge_id',
            'edge_note',
            'edge_id'
            );
        
        // add foreign key for table `note`
        $this->addForeignKey(
            'fk-edge_note-edge_id',
            'edge_note',
            'edge_id',
            'edge',
            'id',
            'CASCADE'
            );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `note`
        $this->dropForeignKey(
            'fk-edge_note-edge_id',
            'edge_note'
            );
        
        // drops index for column `edge_id`
        $this->dropIndex(
            'idx-edge_note-edge_id',
            'edge_note'
            );

        // drops foreign key for table `note`
        $this->dropForeignKey(
            'fk-edge_note-note_id',
            'edge_note'
        );

        // drops index for column `note_id`
        $this->dropIndex(
            'idx-edge_note-note_id',
            'edge_note'
        );

        $this->dropTable('edge_note');
    }
}
