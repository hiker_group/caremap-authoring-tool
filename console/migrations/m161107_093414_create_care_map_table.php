<?php

use yii\db\Migration;

/**
 * Handles the creation of table `care_map`.
 */
class m161107_093414_create_care_map_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('care_map', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'description' => $this->text()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('care_map');
    }
}
