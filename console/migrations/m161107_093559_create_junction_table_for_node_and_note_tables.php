<?php

use yii\db\Migration;

/**
 * Handles the creation of table `node_note`.
 * Has foreign keys to the tables:
 *
 * - `node`
 * - `note`
 */
class m161107_093559_create_junction_table_for_node_and_note_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('node_note', [
            'node_id' => $this->integer(),
            'note_id' => $this->integer(),
            'PRIMARY KEY(node_id, note_id)',
        ], $tableOptions);

        // creates index for column `node_id`
        $this->createIndex(
            'idx-node_note-node_id',
            'node_note',
            'node_id'
        );

        // add foreign key for table `node`
        $this->addForeignKey(
            'fk-node_note-node_id',
            'node_note',
            'node_id',
            'node',
            'id',
            'CASCADE'
        );

        // creates index for column `note_id`
        $this->createIndex(
            'idx-node_note-note_id',
            'node_note',
            'note_id'
        );

        // add foreign key for table `note`
        $this->addForeignKey(
            'fk-node_note-note_id',
            'node_note',
            'note_id',
            'note',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `node`
        $this->dropForeignKey(
            'fk-node_note-node_id',
            'node_note'
        );

        // drops index for column `node_id`
        $this->dropIndex(
            'idx-node_note-node_id',
            'node_note'
        );

        // drops foreign key for table `note`
        $this->dropForeignKey(
            'fk-node_note-note_id',
            'node_note'
        );

        // drops index for column `note_id`
        $this->dropIndex(
            'idx-node_note-note_id',
            'node_note'
        );

        $this->dropTable('node_note');
    }
}
