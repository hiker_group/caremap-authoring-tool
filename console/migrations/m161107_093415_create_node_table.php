<?php

use yii\db\Migration;

/**
 * Handles the creation of table `node`.
 */
class m161107_093415_create_node_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('node', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'care_map_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->createIndex(
            'idx-node-care_map_id',
            'node',
            'care_map_id'
            );
        
        $this->addForeignKey(
            'fk-node-care_map_id',
            'node',
            'care_map_id',
            'care_map',
            'id',
            'CASCADE'
            );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-node-care_map_id',
            'node'
            );
        
        $this->dropIndex(
            'idx-node-care_map_id',
            'node'
            );
        
        $this->dropTable('node');
    }
}
