<?php

use yii\db\Migration;

/**
 * Handles adding active to table `care_map`.
 */
class m161117_154740_add_active_column_to_care_map_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('care_map', 'active', $this->boolean()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('care_map', 'active');
    }
}
