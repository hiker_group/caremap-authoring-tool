<?php

use yii\db\Migration;

/**
 * Handles the creation of table `edge`.
 */
class m161107_093429_create_edge_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('edge', [
            'id' => $this->primaryKey(),
            'previous_node_id' => $this->integer(),
            'next_node_id' => $this->integer(),
            'patients' => $this->integer()->notNull(),
        ], $tableOptions);
        

        // creates index for column `node_id`
        $this->createIndex(
            'idx-edge-previous_node_id',
            'edge',
            'previous_node_id'
            );
        
        // add foreign key for table `node`
        $this->addForeignKey(
            'fk-edge-previous_node_id',
            'edge',
            'previous_node_id',
            'node',
            'id',
            'CASCADE'
            );
        
        // creates index for column `next_node_id`
        $this->createIndex(
            'idx-edge-next_node_id',
            'edge',
            'next_node_id'
            );
        
        // add foreign key for table `node`
        $this->addForeignKey(
            'fk-edge-next_node_id',
            'edge',
            'next_node_id',
            'node',
            'id',
            'CASCADE'
            );
    }
    
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-edge-previous_node_id',
            'edge'
            );
    
        $this->dropIndex(
            'idx-edge-previous_node_id',
            'edge'
            );
    
        $this->dropForeignKey(
            'fk-edge-next_node_id',
            'edge'
            );
    
        $this->dropIndex(
            'idx-edge-next_node_id',
            'edge'
            );
    
        $this->dropTable('edge');
    }
}
