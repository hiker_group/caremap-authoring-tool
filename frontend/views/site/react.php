<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\widgets\authoring\AuthoringAsset;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CareMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'CareMap Authoring');

AuthoringAsset::register($this);
?>
<div class="care-map-index">
    <?= Html::tag('div', '', [
        'id' => 'content',
        'data-caremaps-url' => Url::to(['care-map/all']),
        'data-set-inactive-url' => Url::to(['care-map/set-inactive']),
        'data-save-node-url' => Url::to(['care-map/save-node'])
    ])?>
</div>
