<?php

use yii\helpers\Html; 
use yii\widgets\ActiveForm; 
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */ 
/* @var $model common\models\Edge */ 
/* @var $form ActiveForm */ 
/* @var $nodes common\models\Node[] */ 

?> 
<div class="care-map-add-edge"> 

    <?php $form = ActiveForm::begin(); ?> 

        <?= $form->field($model, 'previous_node_id')->dropDownList(ArrayHelper::map($nodes, 'id', 'name')) ?> 
        <?= $form->field($model, 'next_node_id')->dropDownList(ArrayHelper::map($nodes, 'id', 'name')) ?> 
        <?= $form->field($model, 'patients') ?> 
     
        <div class="form-group"> 
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?> 
        </div> 
    <?php ActiveForm::end(); ?> 

</div><!-- care-map-add-edge --> 
