<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CareMap */

$this->title = Yii::t('app', 'Create Care Map');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Care Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="care-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
