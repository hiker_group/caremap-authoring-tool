<?php

namespace frontend\controllers;

use Yii;
use common\models\CareMap;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\search\CareMapSearch;
use common\models\Node;
use common\models\Edge;
use yii\web\Response;
use yii\filters\AccessControl;

class CareMapController extends \yii\web\Controller
{  
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all CareMap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CareMapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSaveNode()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
    
            $model = new Node();
            if ($model->load(Yii::$app->request->post())) {
                $model->save();
            }
            
            
            Yii::error($model);
            
            return [
                'nodes' => Node::findAll(['care_map_id' => $model->care_map_id])
            ];
        }
    }
    
    public function actionCreate()
    {
        $model = new CareMap();
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CareMap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Displays a single CareMap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $nodes = $model->nodes;
        
        return $this->render('view', [
            'model' => $model,
            'nodes' => $nodes,
        ]);
    }
    
    public function actionAll()
    {       
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $model = new CareMap();
            if ($model->load(Yii::$app->request->post())) {
                $model->save();
            }
            
            return [
                'careMaps' => $this->findUsersCareMaps()
            ];
        }
    }
    
    /**
     * @return \yii\db\ActiveRecord[]
     */
    protected function findUsersCareMaps()
    {
        return CareMap::find()->where(['created_by' => Yii::$app->user->id])->all();
    }

    /**
     * Deletes an existing CareMap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionSetInactive()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $model = $this->findModel(Yii::$app->request->post('id'));
            $model->active = 0;
            
            return [
                'success' => $model->save()
            ];
        }
    }
    
    /**
     * @param integer $id CareMap id
     * @return mixed
     */
    public function actionAddNode($id)
    {
        $model = new Node();
        $model->care_map_id = $id;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('add-node', [
                'model' => $model,
            ]);
        }
    }
    

    /**
     * @param integer $id Node id
     * @return mixed
     */
    public function actionViewNode($id)
    {
        $model = new Node();
        $model->care_map_id = $id;
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('add-node', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionAddEdge($id)
    {
        $model = new Edge();
        
        $careMap = $this->findModel($id);
        $nodes = $careMap->nodes;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('add-edge', [
                'model' => $model,
                'nodes' => $nodes,
            ]);
        }
    }
    
    public function actionReact()
    {
        return $this->render('react');
    }
    
    
//     /**
//      * @param integer $id
//      * @return mixed
//      */
//     public function actionAddNode($id)
//     {
//         $model = new Node();
//         $model->id = $id;
    
//         if ($model->load(Yii::$app->request->post())) {
//             $model->id = $id;
//             if ($model->save()) {
    
//                 return $this->redirect(['view', 'id' => $id]);
//             }
//         }
    
//         return $this->render('add-node', [
//             'model' => $model,
//         ]);
//     }
    
    /**
     * Finds the CareMap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CareMap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CareMap::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
